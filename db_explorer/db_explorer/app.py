
import json
import urllib.parse
import os
import re

from flask import Flask, render_template
from flask_pymongo import PyMongo

app = Flask(__name__)
app.config["MONGO_URI"] = os.environ["MONGO_URI"]
mongo = PyMongo(app)

LOCALIZATION_JSON = {}
with open("./db_explorer/en.json") as f:
    LOCALIZATION_JSON = json.load(f)

@app.template_filter('filter_vtt')
def filter_vtt(text):
    compendium_re = re.compile(r"@Compendium\[(?P<ref>[^\]]+)\]\{(?P<value>[^\}]+)\}")
    def compendium_return(m):
        # TODO: Actually add links
        return f"<a>{m.group('value')}</a>"
    macro_re = re.compile(r"\[\[\/\S*?r(?:oll)?\s(?:(?P<sdice>[^\{\] ]+)[^\{\]]*|(?:\{(?P<ldice>[^\}]+)\}\[[^\]]+\])?)\]\](?:\{(?P<alt>[^\}]+)\})?")
    def macro_return(m):
        if m.group('alt') is not None:
            return m.group('alt')
        return m.group('sdice')
    localize_re = re.compile(r"@Localize\[([^\]]+)\]")
    def localize_return(m):
        path = m.group(1)
        output = LOCALIZATION_JSON
        for chunk in path.split('.'):
            output = output.get(chunk, {})
        if type(output) != str:
            output = f"[LOCALIZATION LOOKUP ERROR - {m.group(0)}]"
        return output
    # Template
    text = localize_re.sub(localize_return, text) # Has to be run before Compendium and Macro replacement
    text = compendium_re.sub(compendium_return, text)
    text = macro_re.sub(macro_return, text)
    return text

@app.route("/")
def list_types():
    types = [ {"name": "Actions", "href": "action"} ]
    return render_template("list.html", header="All types", items=types)

@app.route("/action")
def list_actions():
    items = list(map(lambda x: {"name": x["name"], "href": "./action/" + urllib.parse.quote(x["name"], safe='')}, mongo.db.action.find({}, {"name": 1}).sort("name", 1)))
    return render_template("list.html", header="All actions", items=items)

@app.route("/action/<name>")
def show_action(name):
    item = mongo.db.action.find_one_or_404({"name": name})
    # Actions
    if item["data"]["actions"]["value"] == None:
        if item["data"]["actionType"]["value"] == 'passive':
            item['_action_icon'] = None
        elif item["data"]["actionType"]["value"] == 'reaction':
            item['_action_icon'] = 'r'
        elif item["data"]["actionType"]["value"] == 'free':
            item['_action_icon'] = 0
        else:
            raise ValueError(f'Unknown action type: {item["data"]["actionType"]["value"]}')
    elif item["data"]["actions"]["value"] in (1,2,3):
        item['_action_icon'] = item["data"]["actions"]["value"]
    else:
        raise ValueError(f'Unknown action: {item["data"]["actionType"]["value"]} - {item["data"]["actions"]["value"]}')
    # Traits
    _traits = item.get("data", {}).get("traits", {})
    item["_all_traits"] = []
    item["_all_traits"].append(_traits.get("rarity", {}).get("value", "unkown rarity"))
    item["_all_traits"] += _traits.get("value", [])
    item["_all_traits"] += _traits.get("custom").split(',')
    item["_all_traits"] = list(filter(lambda x: x != '', item["_all_traits"]))
    return render_template("action.html", action=item)

if __name__ == "__main__":
    app.run()